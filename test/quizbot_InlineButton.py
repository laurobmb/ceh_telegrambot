#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging, yaml, random

from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler


from telegram import (
    Poll,
    ParseMode,
    KeyboardButton,
    KeyboardButtonPollType,
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
    Update,
    error,
)
from telegram.ext import (
    Updater,
    CommandHandler,
    PollAnswerHandler,
    PollHandler,
    MessageHandler,
    Filters,
    CallbackContext,
)

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)
logger = logging.getLogger(__name__)


def yaml_loader(arquivo):
    with open(arquivo, "r") as stream:
        try:
            data = yaml.load(stream, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)
    return data


def check_variavel(update, context):
    try:
        first_name = update._effective_user.first_name
        username = update._effective_user.username
        chat_id = update.message.chat.id
        text = update.message.text
        is_bot = update._effective_user.is_bot
    except:
        first_name = update._effective_user.first_name
        username = update._effective_user.username
        chat_id = update.callback_query.message.chat.id
        text = update.callback_query.message.text
        is_bot = update._effective_user.is_bot

    return first_name, username, chat_id, text, is_bot


def start(update: Update, context: CallbackContext) -> None:
    botao = InlineKeyboardButton('quiz', callback_data='quiz')
    buttons_list = [[botao]]
    reply_markup = InlineKeyboardMarkup(buttons_list)
    update.message.reply_text('Vamos Jogar !!!', reply_markup=reply_markup)


def quiz(update: Update, context: CallbackContext) -> None:
    first_name, username, chat_id, text, is_bot = check_variavel(update, context)
    arquivo = "quizbot_dev.yaml"
    #arquivo = "quizbot_homol.yaml"
    #arquivo = "quizbot_prod.yaml"
    d1 = yaml_loader(arquivo)
    d1 = random.choice(d1)
    resposta00 = d1['questao']['respostas'][0]
    resposta01 = d1['questao']['respostas'][1]
    resposta02 = d1['questao']['respostas'][2]
    resposta03 = d1['questao']['respostas'][3]
    questions = [resposta00, resposta01, resposta02, resposta03]

    botao = InlineKeyboardButton('quiz', callback_data='quiz')
    buttons_list = [[botao]]
    reply_markup = InlineKeyboardMarkup(buttons_list)

    for i in questions:
        if d1['questao']['resposta_correta'] == i:
            resposta_correta = questions.index(i)

    try:
        message = update.effective_message.reply_poll(d1['questao']['pergunta'],
                                                      questions,
                                                      type=Poll.QUIZ,
                                                      correct_option_id=resposta_correta)
        payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
        context.bot_data.update(payload)
        logger.info("USER: {} USERNAME: {} ID: {} PERGUNTA: {} MESSAGE: {} BOT: {}".format(
            first_name,
            username,
            chat_id,
            d1['questao']['pergunta'],
            text,
            is_bot))
        context.bot.sendMessage(chat_id=chat_id, disable_notification=True, text='mais uma',reply_markup=reply_markup)


    except error.BadRequest as e:
        context.bot.sendMessage(chat_id=chat_id, disable_notification=True, text=d1['questao']['pergunta'])
        context.bot.sendMessage(chat_id=chat_id, disable_notification=True, text='A - '+resposta00)
        context.bot.sendMessage(chat_id=chat_id, disable_notification=True, text='B - '+resposta01)
        context.bot.sendMessage(chat_id=chat_id, disable_notification=True, text='C - '+resposta02)
        context.bot.sendMessage(chat_id=chat_id, disable_notification=True, text='D - '+resposta03)
        context.bot.sendMessage(chat_id=chat_id, disable_notification=True, text='RESPOSTA CORRETA - '+ d1['questao']['resposta_correta'],reply_markup=reply_markup)
        logger.info("USER: {} USERNAME: {} ID: {} PERGUNTA: {} MESSAGE: {} BOT: {} ERROR: {}".format(
            first_name,
            username,
            chat_id,
            d1['questao']['pergunta'],
            text,
            is_bot,
            e)
        )


def receive_quiz_answer(update: Update, context: CallbackContext) -> None:

    if update.poll.is_closed:
        return
    if update.poll.total_voter_count == 3:
        try:
            quiz_data = context.bot_data[update.poll.id]
        except KeyError:
            return
        context.bot.stop_poll(quiz_data["chat_id"], quiz_data["message_id"])


def receive_poll(update: Update, context: CallbackContext) -> None:
    actual_poll = update.effective_message.poll
    update.effective_message.reply_poll(
        question=actual_poll.question,
        options=[o.text for o in actual_poll.options],
        is_closed=True,
        reply_markup=ReplyKeyboardRemove(),
    )


def help_handler(update: Update, context: CallbackContext) -> None:
    """Display a help message"""
    update.message.reply_text("Use /quiz, /poll or /preview to test this " "bot.")


def button(update, context):
    query = update.callback_query
    if query.data == 'quiz':
        quiz(update,context)


def main() -> None:
    updater = Updater("1557972479:AAGozW4cfbb2MNJwFaObQeDfFc3wPTk5fUg", use_context=True)
    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('quiz', quiz))
    dispatcher.add_handler(PollHandler(receive_quiz_answer))
    dispatcher.add_handler(CommandHandler('help', help_handler))

    updater.dispatcher.add_handler(CallbackQueryHandler(button))
    updater.dispatcher.add_error_handler(error)
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
