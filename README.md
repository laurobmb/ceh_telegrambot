# Bot TELEGRAM para estudar CEH

# Token Telegram

You can export token of Telegram for activate bot on quizbot.py app

    export TOKEN="1557972479:AAGozW4cfbb2MNJwFaObQeDfFc3wPTk5fUg"

or

You can put the token in Dockerfile to execute bot on container

    ENV TOKEN="1557972479:AAGozW4cfbb2MNJwFaObQeDfFc3wPTk5fUg"

## Build image

    buildah bud --layers=true -t ceh:latest .    

## Run image

    podman run ceh


## Reference

[Token Telegram](https://medium.com/shibinco/create-a-telegram-bot-using-botfather-and-get-the-api-token-900ba00e0f39)

[Telegram Bot API](https://core.telegram.org/bots/api)

[Bots: An introduction for developers](https://core.telegram.org/bots)
