FROM python:3.8-slim 

COPY requirements.txt /
RUN pip install -r requirements.txt

COPY files /files

COPY quizbot.py /

ENV TOKEN="1557972479:AAGozW4cfbb2MNJwFaObQeDfFc3wPTk5fUg"
#possible values is "prod" and "dev"
ENV CEH="prod"

CMD [ "python","quizbot.py" ]