import yaml 

def yaml_loader(file):
    with open(file, "r") as stream:
        try:
            data = yaml.load(stream, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)
    return data

def lerquestoes(file):
    d1 = yaml_loader(file)
    while True:
        yield d1

def start():
    questoes= lerquestoes(arquivo)
    nmax = len(next(questoes))
    cont = 0
    while (cont < nmax):
        print(next(questoes)[cont]['questao']['pergunta'],'-',next(questoes)[cont]['questao']['resposta_correta'])
        cont += 1

if __name__ == '__main__':
    arquivo = "files/quizbot_dev.yaml"
    start()