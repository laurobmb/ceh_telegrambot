#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging, yaml, random, os

from telegram import (
    Poll,
    ParseMode,
    KeyboardButton,
    KeyboardButtonPollType,
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
    Update,
    error,
    ChatAction
)
from telegram.ext import (
    Updater,
    CommandHandler,
    PollAnswerHandler,
    PollHandler,
    MessageHandler,
    Filters,
    CallbackContext,
)

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)
logger = logging.getLogger(__name__)

def lerquestoes(FILE):
    d1 = yaml_loader(FILE)
    while True:
        yield d1


def yaml_loader(FILE):
    with open(FILE, "r") as stream:
        try:
            data = yaml.load(stream, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)
    return data


def check_variavel(update, context):
    try:
        first_name = update._effective_user.first_name
        username = update._effective_user.username
        chat_id = update.message.chat.id
        text = update.message.text
        is_bot = update._effective_user.is_bot
    except:
        first_name = update._effective_user.first_name
        username = update._effective_user.username
        chat_id = update.callback_query.message.chat.id
        text = update.callback_query.message.text
        is_bot = update._effective_user.is_bot

    return first_name, username, chat_id, text, is_bot


def start(update: Update, context: CallbackContext) -> None:
    reply_keyboard = [[('/help'),('/quiz'),('/archive')]]
    update.message.reply_text('Vamos começar !!!! \n' 
                               'Use o botão quiz do seu teclado',
                               reply_markup=ReplyKeyboardMarkup(reply_keyboard, 
                               resize_keyboard=True, 
                               one_time_keyboard=False))


def quiz(update: Update, context: CallbackContext) -> None:
    first_name, username, chat_id, text, is_bot = check_variavel(update, context)

    if ceh == "prod":
        arquivo = "files/quizbot_prod.yaml"
    elif ceh == "dev":
        arquivo = "files/quizbot_dev.yaml"

    d1 = yaml_loader(arquivo)
    d1 = random.choice(d1)
    resposta00 = d1['questao']['respostas'][0]
    resposta01 = d1['questao']['respostas'][1]
    resposta02 = d1['questao']['respostas'][2]
    resposta03 = d1['questao']['respostas'][3]
    questions = [resposta00, resposta01, resposta02, resposta03]

    for i in questions:
        if d1['questao']['resposta_correta'] == i:
            resposta_correta = questions.index(i)

    try:
        message = update.effective_message.reply_poll(d1['questao']['pergunta'],
                                                      questions,
                                                      type=Poll.QUIZ,
                                                      correct_option_id=resposta_correta)
        payload = {message.poll.id: {"chat_id": update.effective_chat.id, "message_id": message.message_id}}
        context.bot_data.update(payload)
        logger.info("USER: {} USERNAME: {} ID: {} PERGUNTA: {} MESSAGE: {} BOT: {}".format(
            first_name,
            username,
            chat_id,
            d1['questao']['pergunta'],
            text,
            is_bot))

    except error.BadRequest as e:
        context.bot.sendMessage(chat_id=chat_id, disable_notification=True, text=d1['questao']['pergunta'])
        context.bot.sendMessage(chat_id=chat_id, disable_notification=True, text='A - '+resposta00)
        context.bot.sendMessage(chat_id=chat_id, disable_notification=True, text='B - '+resposta01)
        context.bot.sendMessage(chat_id=chat_id, disable_notification=True, text='C - '+resposta02)
        context.bot.sendMessage(chat_id=chat_id, disable_notification=True, text='D - '+resposta03)
        context.bot.sendMessage(chat_id=chat_id, disable_notification=True, text='RESPOSTA CORRETA - '+ d1['questao']['resposta_correta'])
        logger.info("USER: {} USERNAME: {} ID: {} PERGUNTA: {} MESSAGE: {} BOT: {} ERROR: {}".format(
            first_name,
            username,
            chat_id,
            d1['questao']['pergunta'],
            text,
            is_bot,
            e)
        )


def receive_quiz_answer(update: Update, context: CallbackContext) -> None:

    if update.poll.is_closed:
        return
    if update.poll.total_voter_count == 3:
        try:
            quiz_data = context.bot_data[update.poll.id]
        except KeyError:
            return
        context.bot.stop_poll(quiz_data["chat_id"], quiz_data["message_id"])


def receive_poll(update: Update, context: CallbackContext) -> None:
    actual_poll = update.effective_message.poll
    update.effective_message.reply_poll(
        question=actual_poll.question,
        options=[o.text for o in actual_poll.options],
        is_closed=True,
        reply_markup=ReplyKeyboardRemove(),
    )


def help_handler(update: Update, context: CallbackContext) -> None:
    
    if ceh == "prod":
        arquivo = "files/quizbot_prod.yaml"
    elif ceh == "dev":
        arquivo = "files/quizbot_dev.yaml"

    questoes= lerquestoes(arquivo)
    nmax = len(next(questoes))

    update.message.reply_text(
        "Use /quiz to start new questions for file use /archive.\n"
        "For contact, send message for @laurobmb\n"
        "Exists {} askings\n"
        "now is mode of {}".format(nmax,ceh))


def archive(update: Update, context: CallbackContext) -> None:
    first_name, username, chat_id, text, is_bot = check_variavel(update, context)
    context.bot.send_chat_action(chat_id=chat_id, action=ChatAction.TYPING)
    context.bot.sendMessage(
        chat_id=chat_id,
        disable_notification=True,
        text=('[PROVE YOURSELF](https://exam.proveyourself.net/)'),
        parse_mode=ParseMode.MARKDOWN_V2
        )
    context.bot.send_document(chat_id=chat_id, document=open('files/cehv10.pdf', 'rb'))
    context.bot.send_document(chat_id=chat_id, document=open('files/cehv11.pdf', 'rb'))

def main() -> None:
    updater = Updater(token, use_context=True)
    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('quiz', quiz))
    dispatcher.add_handler(CommandHandler('archive', archive))
    dispatcher.add_handler(PollHandler(receive_quiz_answer))
    dispatcher.add_handler(CommandHandler('help', help_handler))
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    token = os.environ['TOKEN']
    ceh = os.environ['CEH']
    main()
